1) Integration Branch
: Should be maintained in stable condition; can be released anytime
: Corresponding operations should be always stable

2) Topic Branch (Feature Branch)
: Function add or bug fix for Integration Branch
: Created from Integration Branch
: After successful job, Topic Branch is added to Integration Branch

Conversion between branches
1) BASIC branch selected :: 'master'
2) Convert into the other branch is done by 'checkout' command.
3) Commits after checkout is added to the converted branch

HEAD
1) HEAD of the current selected branch.
e.g. 'master' HEAD :: BASIC selection without any checkout nor move.
-> currently working location is presented with 'branch name' and 'HEAD'
2) ~ :: TILDE :: Represent distinct commit location.
3) ^ :: CARET :: Represent distinct commit origins.

STASH
checkout -> branch conversion -> changes not comitted yet; stored as stash.


